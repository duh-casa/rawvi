#!/bin/bash
>&2 echo "First parameter input file, second parameter offset"
if [ $# -lt 2 ] ; then
  SKIP=0
else
  SKIP=$2
fi
dd if=blank.bmp bs=1 count=986
dd if=$1 bs=1024 iflag=fullblock count=1024 skip=$SKIP
echo -ne '\x0a'
