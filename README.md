# RawVi

RAW data VIsualiser -- for viewing raw contents of data storage devices

## Dependencies

* ImageMagick display

## Development plan

### Proof of concept

1. Generate BMP header (using blank.bmp)
2. Add 1 MB of raw data from file, taking offset as parameter
3. pipe to display

### Future

* Create interface for choosing offset (%?)
* Add support for reading raw memory
* Play multiple images in sequence as video stream using FFmpeg ffplay

